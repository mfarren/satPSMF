//
//  main.cpp
//  Project 1 Theory of Computing
//
//  Created by Pedro Saunero & Michael Farren.
//

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <bitset>
#include <cmath>
#include <sys/time.h>
using namespace std;

struct ProblemInfo{
    int problemNum;
    int maxLiterals;
    int nLiterals;
    int nVariables;
    int nClauses;
    char satisfiable;
    int matchesAnswer;
    long time;
    int nSatisfiable = 0;
    int nUnsatisfiable = 0;
    int nHadAnswers = 0;
    int nCorrect = 0;
};

bool DEBUGON = false;

bool getWFF(vector<vector<int> > &wff, ProblemInfo &info, ifstream &file);
vector<bool> getNextAssignment(long long int n, int nVar);
bool verify(vector<bool> assignment, vector<vector<int> > &wff);
vector<string> split(string &line, char delim);
void printResult(bool satisfiable, ProblemInfo &info, vector<bool> &result, ofstream &out);

int main(int argc, const char * argv[]) {
    if(argc<=1){
        cout<<"Filename missing"<<endl;
        return 1;
    }
    string filename = argv[1], line;
    if(argc==3) DEBUGON =(atoi(argv[2]) == 1);
    ifstream file;
    file.open(filename);
    ofstream out;
    out.open("brute-output.csv");
    vector<vector<int> > wff;
    ProblemInfo info;
    int nWFF = 0;
    while(getWFF(wff, info, file)){
        nWFF++;
        struct timeval startTime;
        gettimeofday(&startTime, NULL);
        long start_time = startTime.tv_sec * (int)1e6 + startTime.tv_usec;
        bool satisfiable = false;
        vector<bool> result;
        long long int nPossAssignments = pow(2, info.nVariables);
        for(long long int i = 0; i < nPossAssignments; i++){
            vector<bool> assignment = getNextAssignment(i, info.nVariables);
            if(verify(assignment, wff)){
                satisfiable = true;
                result = assignment;
                break;
            }
        }
        
        struct timeval endTime;
        gettimeofday(&endTime, NULL);
        long end_time = endTime.tv_sec * (int)1e6 + endTime.tv_usec;
        
        info.time = end_time-start_time;
    
        printResult(satisfiable, info, result, out);
    }
    out<<split(filename, '.')[0]<<','<<"satPSMF,"<<nWFF<<','<<info.nSatisfiable<<','<<info.nUnsatisfiable<<','<<info.nHadAnswers<<','<<info.nCorrect;
    file.close();
    out.close();
    return 0;
}


bool getWFF(vector<vector<int> > &wff, ProblemInfo &info, ifstream &file){
    wff.clear();
    stringstream ss;
    string line, dummy;
    if(!getline(file, line)) return false;
    ss<<line;
    ss.ignore();
    ss>>info.problemNum>>info.maxLiterals>>info.satisfiable;
    getline(file, line);
    ss.clear();
    ss<<line;
    ss>>dummy;
    ss>>dummy;
    ss>>info.nVariables>>info.nClauses;
    wff.resize(info.nClauses);
    info.nLiterals = 0;
    for(int i = 0; i < info.nClauses; i++){
        getline(file, line);
        vector<string> parts = split(line, ',');
        for(auto n : parts){
            info.nLiterals+=1;
            if(stoi(n)) wff[i].push_back(stoi(n));
        }
    }
    return true;
}


vector<bool> getNextAssignment(long long int n, int nVariables){
    vector<bool> nextA;
    while(n) {
        n&1 ? nextA.push_back(true) : nextA.push_back(false);
        n>>=1;
    }
    nextA.resize(nVariables, false);
    return nextA;
}

bool verify(vector<bool> assignment, vector<vector<int> > &wff){
    for(auto clause : wff){
        bool result = false;
        for(auto var : clause){
            result = result || (var < 0 ? !assignment[abs(var)-1] : assignment[abs(var)-1]);
        }
        if(!result) return false;
    }
    return true;
}

vector<string> split(string &line, char delim) {
    vector<string> parts;
    stringstream ss;
    ss<<line;
    string part;
    while (getline(ss, part, delim)) {
        parts.push_back(part);
    }
    return parts;
}

void printResult(bool satisfiable, ProblemInfo &info, vector<bool> &result, ofstream &out){
    
    if(info.satisfiable == '?'){
        info.matchesAnswer = 0;
    }else if(info.satisfiable == 'S'){
        info.nHadAnswers+=1;
        info.matchesAnswer = satisfiable ? 1 : -1;
        if(satisfiable) info.nCorrect+=1;
    }else if(info.satisfiable == 'U'){
        info.nHadAnswers+=1;
        info.matchesAnswer = !satisfiable ? 1 : -1;
        if(!satisfiable) info.nCorrect+=1;
    }
    
    if(satisfiable)
        info.nSatisfiable+=1;
    else
        info.nUnsatisfiable+=1;
    
    out<<info.problemNum<<','<<info.nVariables<<','<<info.nClauses<<','<<info.maxLiterals<<','<<info.nLiterals<<
    ','<<info.satisfiable<<','<<info.matchesAnswer<<','<<info.time;
    if(satisfiable){
        for(auto r : result){
            out<<',';
            out<<r;
        }
    }
    out<<endl;
}




