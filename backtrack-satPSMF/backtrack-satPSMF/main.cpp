//  main.cpp
//  backtrack-satPSMF
//
//  Created by Pedro Saunero on 9/25/17.


#include <iostream>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>
#include <sys/time.h>
#include <stack>
using namespace std;

struct ProblemInfo{
    int problemNum;
    int maxLiterals;
    int nLiterals;
    int nVariables;
    int nClauses;
    char satisfiable;
    int matchesAnswer;
    long time;
    int nSatisfiable = 0;
    int nUnsatisfiable = 0;
    int nHadAnswers = 0;
    int nCorrect = 0;
};

struct variables {
  int var;
  int value;
  int num_switch;
  void make_variable(int x) {
    var = x;
    value = 0;
    num_switch = 0;
  }
  void change_value() {
    if (value == 1) { value = 0; }
    else if (value == 0) { value = 1; }
    num_switch += 1;
  }
};

bool DEBUGON = false;

vector<string> split(string &line, char delim);
bool getWFF(vector<vector<int> > &wff, ProblemInfo &info, ifstream &file);
void printResult(bool satisfiable, ProblemInfo &info, vector<int> &result, ofstream &out);
bool verify_true(vector<int> assignment, vector<vector<int> > &wff);
bool clause_false(vector<int> assignment, vector<vector<int> > &wff);

int main(int argc, const char * argv[]) {
    
    if(argc<=1){
        cout<<"Filename missing"<<endl;
        return 1;
    }
    string filename = argv[1], line;
    ifstream file;
    file.open(filename);
    
    if(argc==3) DEBUGON =(atoi(argv[2]) == 1);
    
    ofstream out;
    string fileout = split(filename, '.')[0] + ".csv";
    out.open(fileout);
    
    vector<vector<int> > wff;
    ProblemInfo info;
    
    int nWFF = 0;
    while(getWFF(wff, info, file)){
        nWFF++;
        
        struct timeval startTime;
        gettimeofday(&startTime, NULL);
        long start_time = startTime.tv_sec * (int)1e6 + startTime.tv_usec;
        
        bool satisfiable = false;
        vector<int> result;
	result.assign(info.nVariables,-1);        

        // Backtracking Algorithm 
        stack<variables> mystack;
        variables start;
	start.make_variable(1);
	mystack.push(start);
        while (!mystack.empty()) {
	    int currentvar = mystack.top().var;
	    // If we have tried 0 and 1 for a variable
  	    if (mystack.top().num_switch == 2) {
		result[currentvar - 1] = -1;
	        mystack.pop();
		if (!mystack.empty()) { mystack.top().change_value(); }
	        continue;
	    }
	    // add to result vector
	    result[currentvar - 1] = mystack.top().value;
	    // ckeck if all clauses are true
	    if (verify_true(result, wff)) {
	        satisfiable = true;
                break;
            }  
	    // check if there is a clause that is 100% false (if truw, pop & make result null)
	    if ( (clause_false(result, wff)) || ( (currentvar + 1) > info.nVariables) ) {
		mystack.top().change_value();
		continue;
	    }	  
	    // go a step further into the tree, pick another value
	    variables entry;
	    entry.make_variable(currentvar+1);
	    mystack.push(entry);
	    
        }   
        
        struct timeval endTime;
        gettimeofday(&endTime, NULL);
        long end_time = endTime.tv_sec * (int)1e6 + endTime.tv_usec;
        info.time = end_time-start_time;
        
        printResult(satisfiable, info, result, out);
        
    }
    
    out<<split(filename, '.')[0]<<','<<"satPSMF,"<<nWFF<<','<<info.nSatisfiable<<','<<info.nUnsatisfiable<<','<<
    info.nHadAnswers<<','<<info.nCorrect;
    
    file.close();
    out.close();
    
    return 0;
}

bool getWFF(vector<vector<int> > &wff, ProblemInfo &info, ifstream &file){
    wff.clear();
    stringstream ss;
    string line, dummy;
    if(!getline(file, line)) return false;
    ss<<line;
    ss.ignore();
    ss>>info.problemNum>>info.maxLiterals>>info.satisfiable;
    getline(file, line);
    ss.clear();
    ss<<line;
    ss>>dummy;
    ss>>dummy;
    ss>>info.nVariables>>info.nClauses;
    wff.resize(info.nClauses);
    info.nLiterals = 0;
    for(int i = 0; i < info.nClauses; i++){
        getline(file, line);
        vector<string> parts = split(line, ',');
        for(auto n : parts){
            info.nLiterals+=1;
            if(stoi(n)) wff[i].push_back(stoi(n));
        }
    }
    return true;
}

void printResult(bool satisfiable, ProblemInfo &info, vector<int> &result, ofstream &out){
    
    if(info.satisfiable == '?'){
        info.matchesAnswer = 0;
    }else if(info.satisfiable == 'S'){
        info.nHadAnswers+=1;
        info.matchesAnswer = satisfiable ? 1 : -1;
        if(satisfiable) info.nCorrect+=1;
    }else if(info.satisfiable == 'U'){
        info.nHadAnswers+=1;
        info.matchesAnswer = !satisfiable ? 1 : -1;
        if(!satisfiable) info.nCorrect+=1;
    }
    
    if(satisfiable)
        info.nSatisfiable+=1;
    else
        info.nUnsatisfiable+=1;
    
    out<<info.problemNum<<','<<info.nVariables<<','<<info.nClauses<<','<<info.maxLiterals<<','<<info.nLiterals<<
    ','<<(satisfiable ? 'S' : 'U')<<','<<info.matchesAnswer<<','<<info.time;
    if(satisfiable){
        for(auto r : result){
            out<<',';
            out<<r;
        }
    }
    out<<endl;
}


vector<string> split(string &line, char delim) {
    vector<string> parts;
    stringstream ss;
    ss<<line;
    string part;
    while (getline(ss, part, delim)) {
        parts.push_back(part);
    }
    return parts;
}

bool verify_true(vector<int> assignment, vector<vector<int> > &wff) {
    for(auto clause : wff){
        bool result = false;
        for(auto var : clause){
	    if (assignment[abs(var)-1] == -1) { continue; }
            result = result || (var < 0 ? !assignment[abs(var)-1] : assignment[abs(var)-1]);
        }
        if(!result) return false;
    }
    return true;
}

bool clause_false(vector<int> assignment, vector<vector<int> > &wff) {
    for (auto clause : wff) {
        bool result = true;
	for (auto var : clause) {
	    if (assignment[abs(var)-1] == -1) { result = true; }
	    else { result = (var < 0 ? !assignment[abs(var)-1] : assignment[abs(var)-1]); }
	    if (result == true) { break; }
	}
	if(!result) return true;
    } 
    return false;    
}









